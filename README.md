# Gelato CheckOut

## Requirements
Docker

## Setup
Position yourself inside the project directory
```
$ cd gelato
```

Build a docker image
```
$ docker build -t gelato-php .
```

Run docker container
```
$ docker run --name=gelato-php -i -t -d gelato-php
```

## Usage
To save time there is no API or UI for using the application.
So testing is done by running the unit tests.

```
$ docker container exec -it gelato-php ./vendor/bin/phpunit tests
```

Write more tests if the existing tests do not suffice. =)

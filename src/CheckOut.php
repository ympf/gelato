<?php declare(strict_types = 1);

require_once  __DIR__ . '/Rule.php';
require_once  __DIR__ . '/Data.php';

class CheckOut
{
    private array $priceingRules;

    private int $total = 0;

    private array $itemsCount = [];

    public function __construct(array $priceingRules = [])
    {
        $this->priceingRules = $priceingRules;
    }

    public function scan(string $sku): void
    {
        if (!array_key_exists($sku, Data::ITEM_UNIT_PRICES)) {
            throw new Exception(sprintf('Sku %s does not exist.', $sku));
        }

        if (!array_key_exists($sku, $this->itemsCount)) {
            $this->itemsCount[$sku] = 0;
        }

        $this->itemsCount[$sku]++;
    }

    public function total(): float
    {
        $total = 0;

        foreach ($this->itemsCount as $sku => $count) {
            if ($count == 0) {
                continue;
            }

            $amountDiscount = $this->applyAmountDiscountRules($sku, $count);

            $total += Data::ITEM_UNIT_PRICES[$sku] * $count - $amountDiscount;
        }

        $total = $total - $this->applyTotalDiscountRules($total);

        return $total;
    }

    private function applyAmountDiscountRules($sku, $itemCount): float
    {
        if (!array_key_exists(RULE::RULE_TYPE_AMOUNT, $this->priceingRules) ||
            !array_key_exists($sku, $this->priceingRules[RULE::RULE_TYPE_AMOUNT])) {
            return 0;
        }

        $discount = 0;

        foreach ($this->priceingRules[RULE::RULE_TYPE_AMOUNT][$sku] as $rule) {
            $ruleAmount = $rule->getAmount();
            $ruleDiscount = $rule->getDiscount();

            if ($itemCount >= $ruleAmount) {
                $discount = intval($itemCount / $ruleAmount) * $ruleDiscount;
            }
        }

        return $discount;
    }

    private function applyTotalDiscountRules($total): float
    {
        if (!array_key_exists(RULE::RULE_TYPE_TOTAL, $this->priceingRules)) {
            return 0;
        }

        $discount = 0;

        foreach ($this->priceingRules[Rule::RULE_TYPE_TOTAL] as $rule) {
            if ($total >= $rule->getAmount()) {
                $discount = $total - ($total * ((100 - $rule->getDiscount()) / 100));
            }
        }

        return $discount;
    }
}

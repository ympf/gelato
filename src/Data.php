<?php declare(strict_types = 1);

class Data
{
    CONST ITEM_UNIT_PRICES = [
        'A' => 50,
        'B' => 30,
        'C' => 20,
        'D' => 15
    ];
}

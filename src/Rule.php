<?php declare(strict_types = 1);

class Rule
{
    const RULE_TYPE_TOTAL = 'total';

    const RULE_TYPE_AMOUNT = 'amount';

    private int $amount;

    private int $discount;

    public function __construct(int $amount, int $discount)
    {
        $this->amount = $amount;
        $this->discount = $discount;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getDiscount()
    {
        return $this->discount;
    }
}

<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once  __DIR__ . '/../src/CheckOut.php';
require_once  __DIR__ . '/../src/Rule.php';

class CheckOutTest extends TestCase
{
    public function testScan()
    {
        try {
            $co = new CheckOut();

            $co->scan('A');
            $co->scan('B');
            $co->scan('C');
            $co->scan('D');

            $this->assertTrue(true);
        } catch (Exception $e) {
            $this->assertTrue(false, "Scanning should be working.");
        }
    }

    public function testScanFail()
    {
        try {
            $co = new CheckOut([]);
            $co->scan('F');
            $this->assertTrue(false, "F should throw an exception.");
        } catch (Exception $e) {
            $this->assertSame('Sku F does not exist.', $e->getMessage());
        }
    }

    public function testTotalWithoutRules()
    {
        $co = new CheckOut();

        $co->scan('D');
        $co->scan('C');

        $this->assertSame($co->total(), 35.0);

        $co->scan('A');
        $co->scan('C');

        $this->assertSame($co->total(), 105.0);
    }

    public function testTotalWithRules()
    {
        $pricingRules = [
            RULE::RULE_TYPE_AMOUNT => [
                'A' => [
                    new Rule(3, 20)
                ],
                'B' => [
                    new Rule(2, 15)
                ],
            ]
        ];

        $co = new CheckOut($pricingRules);

        $co->scan('B');
        $co->scan('A');
        $co->scan('B');

        $this->assertSame($co->total(), 95.0);

        $co->scan('A');
        $co->scan('A');

        $this->assertSame($co->total(), 175.0);

        $co->scan('C');
        $co->scan('A');

        $this->assertSame($co->total(), 245.0);
    }

    public function testTotalWithMultiplRulesForSameSku()
    {
        $pricingRules = [
            RULE::RULE_TYPE_AMOUNT => [
                'A' => [
                    new Rule(3, 20),
                    new Rule(6, 50)
                ]
            ]
        ];

        $co = new CheckOut($pricingRules);

        $co->scan('A');
        $co->scan('A');
        $co->scan('A');

        $this->assertSame($co->total(), 130.0);

        $co->scan('A');
        $co->scan('A');
        $co->scan('A');

        $this->assertSame($co->total(), 250.0);
    }

    public function testTotalWithMultiplePercentageRules()
    {
        $pricingRules = [
            Rule::RULE_TYPE_TOTAL => [
                new Rule(200, 10),
                new Rule(500, 20)
            ]
        ];

        $co = new CheckOut($pricingRules);

        $co->scan('A');
        $co->scan('A');
        $co->scan('A');
        $co->scan('A');

        $this->assertSame($co->total(), 180.0);

        $co->scan('A');
        $co->scan('A');
        $co->scan('A');
        $co->scan('A');
        $co->scan('A');
        $co->scan('A');

        $this->assertSame($co->total(), 400.0);
    }

    public function testTotalWithPercentageAndAmountRules()
    {
        $pricingRules = [
            Rule::RULE_TYPE_TOTAL => [
                new Rule(200, 10)
            ],
            RULE::RULE_TYPE_AMOUNT => [
                'A' => [
                    new Rule(3, 20),
                    new Rule(6, 50)
                ]
            ]
        ];

        $co = new CheckOut($pricingRules);

        $co->scan('A');
        $co->scan('A');
        $co->scan('A');
        $co->scan('A');

        $this->assertSame($co->total(), 180.0);

        $co->scan('A');

        $this->assertSame($co->total(), 207.0);

        $co->scan('A');

        $this->assertSame($co->total(), 225.0);
    }
}
